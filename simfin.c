#include "parson.h"
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <fcntl.h>
#include <pwd.h>
#include <sys/stat.h>

int main() {
  /* Get the users current home directory for creating the simfin.json file */
  struct stat st = {0};
  struct passwd *pw = getpwuid(getuid());
  /* This gets the homedir so that later I can make the simfin.json file  in /home/username/.simfin */
  char *homeDir = pw->pw_dir;
  /* This just sets up strings so that I can add them later to homeDir and make the full path*/
  /* This is the file name */
  char *simfinFile = "/simfin.json";
  /* This is the file's directory
  When added together, they make the full path*/
  char *simfinDir = "/.simfin";
  strcat(homeDir, simfinDir);
  if (stat(homeDir, &st) == -1) {
    /* Create the .simfin directory if it doesn't already exist */
    mkdir(homeDir, 0700);
  }
  else {
    /* Add filename to homeDir if the .simfin directory already exists*/
    strcat(homeDir,simfinFile);
  }
  /* If the simfin.json file doesn't exist, then create it*/
  if (!access(homeDir, F_OK )) {}
  else {
    /* Create a file with 777 permissions */
    int fd2 = open (homeDir, O_RDWR|O_CREAT,S_IRWXU|S_IRWXG|S_IRWXO);
    if (fd2 != -1) {
      /* Write a json string to simfin.json */
      JSON_Value *notYetSimfin = json_parse_string("{\"money\":\"0\", \"moneyaddrm\":0, \"whenaddrm\":\"00:00 16\\/02\\/16\"}");
      json_serialize_to_file_pretty(notYetSimfin, homeDir);
      json_value_free(notYetSimfin);
    }
  }
  /* Get values from the simfin.json file */
  JSON_Value *simfin = json_parse_file(homeDir);
  char input[256];
  int moneyChanged = json_object_get_number(json_object(simfin), "moneyaddrm");
  int money = json_object_get_number(json_object(simfin), "money");
  const char *whenMoneyChanged = NULL;
  whenMoneyChanged = json_object_get_string(json_object(simfin), "whenaddrm");
  printf("What do you want to do? ");
  scanf("%s", &input);
  /* The following code compares input to a string */
  if (strcmp(input, "howmuch") == 0) {
    printf("You have %d.\n", money);
    printf("Do you want to do anything else? Yes(y) or No(anything else) ");
    scanf("%s", &input);
    /* This asks if the user wants to do anything else */
    if (strcmp(input, "y") == 0) {
      main();
    }
    else {
      json_value_free(simfin);
      return 0;
    }
  }
  else if (strcmp(input, "help") == 0) {
    printf("You are probably here because you want help.\nSo simfin works like this.\nadd to add money and remove to remove it. \nhowmuch to check how much you have and history to check the latest change you made.\n");
    printf("Do you want to do anything else? Yes(y) or No(anything else)");
    scanf("%s", &input);
    if (strcmp(input, "y") == 0) {
      main();
    }
    else {
      json_value_free(simfin);
      return 0;
    }
  }
  else if (strcmp(input, "history") == 0) {
    printf("Last time you added/removed %d at %s\n", moneyChanged, whenMoneyChanged);
    printf("Do you want to anything else? Yes(y) or No(anything else) ");
    scanf("%s", &input);
    if (strcmp(input, "y") == 0)
      main();
    else {
      json_value_free(simfin);
      return 0;
    }
  }
  /* Adds money and sets the date */
  else if (strcmp(input, "add") == 0) {
    printf("How much money do you want to add? ");
    scanf("%d", &moneyChanged);
    money = money + moneyChanged;
    simfin = json_value_init_object();
    json_object_set_number(json_object(simfin), "money", money);
    json_object_set_number(json_object(simfin), "moneyaddrm", moneyChanged);
    /* Gets current time */
    time_t rawtime;
    struct tm * timeinfo;
    char mytime[80];
    time (&rawtime);
    timeinfo = localtime (&rawtime);
    strftime (mytime,80,"%R %d/%m/%y", timeinfo);
    json_object_set_string(json_object(simfin), "whenaddrm", mytime);
    json_serialize_to_file_pretty(simfin, homeDir);
    printf("You now have %d.\n", money);
    printf("Do you want to anything else? Yes(y) or No(anything else) ");
    scanf("%s", &input);
    if (strcmp(input, "y") == 0) {
      main();
    }
    else {
    json_value_free(simfin);
    return 0;
    }
  }
  /* Removes money and sets the date */
  else if (strcmp(input, "remove") == 0) {
    printf("How much money do you want to remove? ");
    scanf("%d", &moneyChanged);
    money = money - moneyChanged;
    simfin = json_value_init_object();
    json_object_set_number(json_object(simfin), "money", money);
    json_object_set_number(json_object(simfin), "moneyaddrm", moneyChanged);
    /* Gets current time */
    time_t rawtime;
    struct tm * timeinfo;
    char mytime[80];
    time (&rawtime);
    timeinfo = localtime (&rawtime);
    strftime (mytime,80,"%R %d/%m/%y",timeinfo);
    json_object_set_string(json_object(simfin), "whenaddrm", mytime);
    json_serialize_to_file_pretty(simfin, homeDir);
    printf("You now have %d.\n", money);
    printf("Do you want to do anything else? Yes(y) or No(anything else) ");
    scanf("%s", &input);
    if (strcmp(input, "y") == 0) {
      main();
    }
    else {
    json_value_free(simfin);
    return 0;
    }
  }
  else {
    /* This message should leave you expecting me to add new features */
    printf("You can't do that with this program. Yet...\n");
    printf("Do you want to do anything else? Yes(y) or No(anything else)? ");
    scanf("%s", &input);
    if (strcmp(input, "y") == 0) {
      main();
    }
    else {
      json_value_free(simfin);
      return 0;
    }
  }
}

