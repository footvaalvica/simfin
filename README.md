#simfin

[![Built with Spacemacs](https://cdn.rawgit.com/syl20bnr/spacemacs/442d025779da2f62fc86c2082703697714db6514/assets/spacemacs-badge.svg)](http://github.com/syl20bnr/spacemacs)

A simple money managing application, if you think other apps have too many features, this one's for you.

Simfin works like this, ```add``` to add money, ```remove```to remove it, ```howmuch``` to check how much you have and ```history``` to check the latest change you made.

I use [parson](https://kgabis.github.io/parson/) for json parsing.

To build the software in Fedora you must install the group "Development Tools" first. You can do it like so `sudo dnf groupinstall "Development Tools"`. After that's done, run `make` in the project directory. To install it run ``sudo make install`` in the project directory and to uninstall it run `sudo make uninstall`.
