CC = gcc
CFLAGS = -O0 -g

all: simfin

install:
	$(CC) $(CFLAGS) -o simfin simfin.c parson.c
	mv simfin /usr/bin/

simfin: simfin.c parson.c
	$(CC) $(CFLAGS) -o $@ simfin.c parson.c

clean:
	rm -f simfin *.o
	rm -rf ~/.simfin

uninstall:
	rm -f /usr/bin/simfin
	rm -rf ~/.simfin
